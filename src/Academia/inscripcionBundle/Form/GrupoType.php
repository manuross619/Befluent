<?php

namespace Academia\inscripcionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GrupoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tipo',ChoiceType::class,array('choices' => array('Ingles' => 'Ingles', 'Frances'=> 'Frances' ),))
        ->add('nivel',ChoiceType::class,array('choices' => array('Basico' => 'Basico', 'Intermedio'=> 'Intermedio','Avanzado'=>'Avanzado' ),))
        ->add('horario')
        ->add('salon')
        ->add('horarioFin')
        ->add('modalidad',ChoiceType::class,array('choices' => array('Intensivo' => 'Intensivo', 'Sabatico'=> 'Sabatico' ),))
        ->add('coach', EntityType::class, array(
            'class' => 'AcademiainscripcionBundle:Usuario',
            'choice_label' => 'nombre',
            'label'=>'Nombre del usuario',
            'multiple'=>false 
             ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Academia\inscripcionBundle\Entity\Grupo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'academia_inscripcionbundle_grupo';
    }


}
