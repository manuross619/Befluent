<?php

namespace Academia\inscripcionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Academia\inscripcionBundle\Entity\Grupo;

class EstudianteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $grupo = new Grupo();
        $builder
        ->add('nombre','text',array('attr'=> array('placeholder'=>'Nombre Estudiante')))
        ->add('edad','integer',array('attr'=> array('placeholder'=>'Edad Estudiante')))
        ->add('dui','text',array('attr'=> array('placeholder'=>'Dui Estudiante')))
        ->add('telefono')
        ->add('email','email',array('attr'=> array('placeholder'=>'correo@dominio.com')))
        //->add('id','entity',array('class'=>Grupo::class,'choice_label'=>'tipo','multiple'=>true,'expanded'=>false));

        //dos formas de
        ->add('grupos',EntityType::class, array(
            'class' => 'AcademiainscripcionBundle:Grupo',
            'choice_label' => function ($grupo) {
                return  $grupo->getTipo() . '  ' . $grupo->getHorario()->format('H:i') . 
                '   -   ' . $grupo->getHorarioFin()->format('H:i') . ' ' . $grupo->getNivel()
                ;;
                },
            'multiple' => true,
            'required'=>true

        ));
       // ->add('grupos');


    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Academia\inscripcionBundle\Entity\Estudiante'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'academia_inscripcionbundle_estudiante';
    }


}
