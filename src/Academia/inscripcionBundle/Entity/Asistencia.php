<?php

namespace Academia\inscripcionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Asistencia
 *
 * @ORM\Table(name="asistencia")
 * @ORM\Entity(repositoryClass="Academia\inscripcionBundle\Repository\AsistenciaRepository")
 */
class Asistencia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var int
     *
     * @ORM\Column(name="tipe_id", type="integer")
     */
    private $tipeId;



    #*********************************** nueva relacion *************************

    /**
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="asistencias")
     *  @ORM\JoinColumn(name="grupo_id", referencedColumnName="id", onDelete="SET NULL")
     */ 
    private $idGrupo; 

    /**
     * @ORM\ManyToOne(targetEntity="Estudiante", inversedBy="asistencias")
     *  @ORM\JoinColumn(name="estud_id", referencedColumnName="id", onDelete="SET NULL")
     */ 
    private $idEstudiante;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Asistencia
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set tipeId
     *
     * @param integer $tipeId
     *
     * @return Asistencia
     */
    public function setTipeId($tipeId)
    {
        $this->tipeId = $tipeId;
    
        return $this;
    }

    /**
     * Get tipeId
     *
     * @return integer
     */
    public function getTipeId()
    {
        return $this->tipeId;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->estudiantes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

    /**
     * Set idGrupo
     *
     * @param \Academia\inscripcionBundle\Entity\Grupo $idGrupo
     *
     * @return Asistencia
     */
    public function setIdGrupo(\Academia\inscripcionBundle\Entity\Grupo $idGrupo = null)
    {
        $this->idGrupo = $idGrupo;
    
        return $this;
    }

    /**
     * Get idGrupo
     *
     * @return \Academia\inscripcionBundle\Entity\Grupo
     */
    public function getIdGrupo()
    {
        return $this->idGrupo;
    }

    /**
     * Set idEstudiante
     *
     * @param \Academia\inscripcionBundle\Entity\Estudiante $idEstudiante
     *
     * @return Asistencia
     */
    public function setIdEstudiante(\Academia\inscripcionBundle\Entity\Estudiante $idEstudiante = null)
    {
        $this->idEstudiante = $idEstudiante;
    
        return $this;
    }

    /**
     * Get idEstudiante
     *
     * @return \Academia\inscripcionBundle\Entity\Estudiante
     */
    public function getIdEstudiante()
    {
        return $this->idEstudiante;
    }
}
