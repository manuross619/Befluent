<?php

namespace Academia\inscripcionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nota
 *
 * @ORM\Table(name="nota")
 * @ORM\Entity(repositoryClass="Academia\inscripcionBundle\Repository\NotaRepository")
 */
class Nota
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", nullable=true)
     */
    private $valor;

/**
    * @ORM\ManyToOne(targetEntity="Evaluacion", inversedBy="nota")
    * @ORM\JoinColumn(name="evaluacion", referencedColumnName="id", onDelete="CASCADE")
    * 
     */

    private $evaluacion;

    /**
    * @ORM\ManyToOne(targetEntity="Estudiante", inversedBy="notaEst")
    * @ORM\JoinColumn(name="estudiante", referencedColumnName="id", onDelete="CASCADE")
    * 
     */

    private $estudiante;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Nota
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    
        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set evaluacion
     *
     * @param \Academia\inscripcionBundle\Entity\Evaluacion $evaluacion
     *
     * @return Nota
     */
    public function setEvaluacion(\Academia\inscripcionBundle\Entity\Evaluacion $evaluacion = null)
    {
        $this->evaluacion = $evaluacion;
    
        return $this;
    }

    /**
     * Get evaluacion
     *
     * @return \Academia\inscripcionBundle\Entity\Evaluacion
     */
    public function getEvaluacion()
    {
        return $this->evaluacion;
    }

    /**
     * Set estudiante
     *
     * @param \Academia\inscripcionBundle\Entity\Estudiante $estudiante
     *
     * @return Nota
     */
    public function setEstudiante(\Academia\inscripcionBundle\Entity\Estudiante $estudiante = null)
    {
        $this->estudiante = $estudiante;
    
        return $this;
    }

    /**
     * Get estudiante
     *
     * @return \Academia\inscripcionBundle\Entity\Estudiante
     */
    public function getEstudiante()
    {
        return $this->estudiante;
    }
}
