<?php

namespace Academia\inscripcionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AcademiainscripcionBundle:Default:index.html.twig');
    }
}
