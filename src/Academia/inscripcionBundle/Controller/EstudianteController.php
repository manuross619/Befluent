<?php

namespace Academia\inscripcionBundle\Controller;

use Academia\inscripcionBundle\Entity\Estudiante;
use Academia\inscripcionBundle\Entity\Inscripcion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Academia\inscripcionBundle\Entity\Grupos;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Estudiante controller.
 *
 */
class EstudianteController extends Controller
{
    /**
     * Lists all estudiante entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        //$estudiantes = $em->getRepository('AcademiainscripcionBundle:Estudiante')->findAll();

        $dql="SELECT e FROM AcademiainscripcionBundle:Estudiante e";
        $estudiantes= $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($estudiantes, $request->query->getInt('page', 1), 5);

        return $this->render('estudiante/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new estudiante entity.
     *
     */
    public function newAction(Request $request)
    {
        $estudiante = new Estudiante();
        $inscripcion = new Inscripcion();



        $form = $this->createForm('Academia\inscripcionBundle\Form\EstudianteType', $estudiante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $inscripcion->setFecha(new \DateTime());
            $inscripcion->setEstudiante($estudiante);
            $em = $this->getDoctrine()->getManager();
            $em->persist($estudiante);
            $em->persist($inscripcion);
            $em->flush();

            return $this->redirectToRoute('estudiante_show', array('id' => $estudiante->getId()));
        }

        return $this->render('estudiante/new.html.twig', array(
            'estudiante' => $estudiante,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a estudiante entity.
     *
     */
    public function showAction(Estudiante $estudiante)
    {
        $deleteForm = $this->createDeleteForm($estudiante);

        return $this->render('estudiante/show.html.twig', array(
            'estudiante' => $estudiante,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing estudiante entity.
     *
     */
    public function editAction(Request $request, Estudiante $estudiante)
    {
        $deleteForm = $this->createDeleteForm($estudiante);
        $editForm = $this->createForm('Academia\inscripcionBundle\Form\EstudianteType', $estudiante);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('estudiante_edit', array('id' => $estudiante->getId()));
        }

        return $this->render('estudiante/edit.html.twig', array(
            'estudiante' => $estudiante,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            
        ));
    }

    /**
     * Deletes a estudiante entity.
     *
     */
    public function deleteAction(Request $request, Estudiante $estudiante)
    {
        $form = $this->createDeleteForm($estudiante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($estudiante);
            $em->flush();
        }

        return $this->redirectToRoute('estudiante_index');
    }

    /**
     * Creates a form to delete a estudiante entity.
     *
     * @param Estudiante $estudiante The estudiante entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Estudiante $estudiante)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('estudiante_delete', array('id' => $estudiante->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    public function obtenerAction(Request $request)
    {
       // $seleccion = isset($_GET['id']) ? $_GET('ID') : $_POST('id');
        //$id=$_POST["id"];
        $curso=$request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $grupos = $em->getRepository('AcademiainscripcionBundle:Grupo')->findBy(array('tipo' => $curso));
        if($request->isXmlHttpRequest()){

            $jsonData = array();
            $idx=0;

            foreach ($grupos as $grupos) {
                # code...
                $temp = array(
                'ids' => $grupos->getId(),  
                'hora' => $grupos->getHorario()->format('H:i'),
                'horaFin' => $grupos->getHorarioFin()->format('H:i'),
                'modalidad' => $grupos->getModalidad(),  
         );  
                $jsonData[$idx++] = $temp;

            }
            return new JsonResponse($jsonData);
           // $encoders = array(new JsonEncoder());
            //$normalizers = array(new ObjectNormalizer());
            //$serializer = new Serializer($normalizers, $encoders);


             
            /* $response = new JsonResponse();
                $response->setData(array('horarios' => $estudiantes
        ));
              return $response;*/
             // $datos=array('est' => $estudiantes[0] );
            // return $this->renderText(json_encode($datos));
        }else { 
            return $this->render('student/ajax.html.twig'); 
        }    
       
    }

    public function consultaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sql="SELECT es FROM AcademiainscripcionBundle:Estudiante es WHERE  ";
        $usuariosRecep= $em->createQuery($sql);

    }

}
