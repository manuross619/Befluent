<?php

namespace Academia\inscripcionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
#use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        
        $error = $authenticationUtils->getLastAuthenticationError();
        
        $lastUsername = $authenticationUtils->getLastUsername();
        
        #renderiza src/UserBundle/Resources/views/Security/login.html.twig
        #return $this->render('UserBundle:Security:login.html.twig', array('last_username' => $lastUsername, 'error' => $error));
        
        #renderiza a la vista login.html.twig que esta en el directorio security todo esto en el directorio 
        # app/Resources/views/security/login.html.twig
        
        
        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername, 
            'error' => $error));

            
        
        #return $this->render('security/logueo.html.twig', array(
         #   'last_username' => $lastUsername, 
          #  'error' => $error));
    }
    
    public function loginCheckAction()
    {
        
    }
}
