<?php

namespace Academia\inscripcionBundle\Controller;

use Academia\inscripcionBundle\Entity\Evaluacion;
use Academia\inscripcionBundle\Entity\Grupo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Form;

/**
 * Evaluacion controller.
 *
 */
class EvaluacionController extends Controller
{
    /**
     * Lists all evaluacion entities.
     *
     */

   
  

    public function indexAction(Grupo $grupo,Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $evaluacions = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $grupo->getId() ));

         $dql="SELECT d FROM AcademiainscripcionBundle:Evaluacion d";
        $evaluaciones= $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($evaluaciones, $request->query->getInt('page', 1), 5);



        return $this->render('evaluacion/index.html.twig', array(
            'evaluacions' => $evaluacions,
            'pagination'=>$pagination,
        ));
    }

    /**
     * Creates a new evaluacion entity.
     *
     */
    public function newAction(Request $request)
    {   
        $em = $this->getDoctrine()->getManager();
        $totalPonderaciones=0;
        if($request->isXmlHttpRequest()){
           
        $i=0;
        $nombre=$request->request->get("nombre");
        $ponderacion=$request->request->get("ponderacion");
        $idGrupo=$request->request->get("idGrupo");
        $tam=$request->request->get("tam");
        $mes=$request->request->get("mes");
        $anio=$request->request->get("anio");
        $grupoObtenido=$em->getRepository('AcademiainscripcionBundle:Grupo')->findBy(array('id' => $idGrupo ));// Se obtiene el grupo al que se le van agregar las evaluaciones
        $grupoP=$grupoObtenido[0];
  

        while ($i<$tam ) {
            # code...
            $evaluaciones=new Evaluacion();
            $evaluaciones->setNombre($nombre[$i]);
            $evaluaciones->setPonderacion($ponderacion[$i]);
            $evaluaciones->setIdGrupo($grupoP);
            $evaluaciones->setPeriodoMes($mes);
            $evaluaciones->setPeriodoAño($anio);
            $em->persist($evaluaciones);
            $em->flush();
            $i++;
        }
        $eval[$i]=$evaluaciones;
        $i++;

        $jsonData = array();
          
                # code...
                $temp = array(
                'nombre' => $nombre,  
                'ponderacion' => $ponderacion);  
         
                $jsonData[0] = $temp;            
            return new JsonResponse($jsonData);

        }

        return $this->render('evaluacion/new.html.twig', array(

        ));
    }

    /**
     * Finds and displays a evaluacion entity.
     *
     */
    public function showAction(Grupo $grupo,Request $request)
    {
        $idGr=$grupo->getId(); 
        $em = $this->getDoctrine()->getManager();
        $now = new  \DateTime();
        $año=$now->format('Y');
        $mes=$now->format('m');
        //$evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $idGr ));
        $evaluaciones1 = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $idGr,'periodoAño'=>$año,'periodoMes'=>$mes));
        $tamaño=sizeof($evaluaciones1);
        $editForm = $this->createFormBuilder($evaluaciones1);
        $editForm = $editForm->getForm();
        $editForm->handleRequest($request);
        $t=sizeof($evaluaciones1);

        //------------------------------------------------------------------------------------------------------>>>>>>>>
        if (isset($_POST["buscar"])||$editForm->isSubmitted()) {

            $mes=$request->request->get("mes");
            $año=$request->request->get("anio");
            $mesC=$this->obtenerMes($mes);
            $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $idGr,'periodoAño'=>$año,'periodoMes'=>$mes));
            
            $now = new  \DateTime();
            $t=sizeof($evaluaciones);
            

        
        

         if ($editForm->isSubmitted()&&$editForm->isValid()) {
            $i=0;
                    foreach ($evaluaciones as $valor) {
                         $nombre= $request->request->get($evaluaciones[$i]->getId().$evaluaciones[$i]->getId());
                         $ponderacion= $request->request->get($evaluaciones[$i]->getId());
                         $id=$evaluaciones[$i]->getIdGrupo();

                         $em = $this->getDoctrine()->getManager();
                         $evaluacionViejo = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->find($evaluaciones[$i]->getId());
                         if ($nombre!=""&&$ponderacion!="") {
                           # code...
                         
                         $evaluacionViejo->setNombre($nombre);
                         $evaluacionViejo->setPonderacion($ponderacion);
                         $em->flush();}
                         //$evaluacionEdita->setIdGrupo($evaluaciones[$i]->getIdGrupo());
                        
                    
                         //$consulta = $em->createQuery('UPDATE AcademiainscripcionBundle:Evaluacion d SET d.nombre = :nombre WHERE d.id = :id');
            
                         //$consulta->setParameters(array('nombre' => $evaluaciones[$i]->getIdGrupo(),'id'  => $nombre,));


                        $i=$i+1;

                    }

            //return $this->redirectToRoute('evaluacion_show',array('id'=>$grupo->getId()));
                    return $this->render('evaluacion/show.html.twig', array(
            //'pagination'=>$pagination,
            'evaluaciones'=>$evaluaciones,
            'editForm'=>$editForm->createView(),
            'tamaño'=>$tamaño,
            'año'=>$año,
            'mes'=>$mes,
            't'=>$t,
        ));
            # code...
        }

        return $this->render('evaluacion/show.html.twig', array(
            //'pagination'=>$pagination,
            'evaluaciones'=>$evaluaciones,
            'editForm'=>$editForm->createView(),
            'tamaño'=>$tamaño,
            'año'=>$año,
            'mes'=>$mes,
            't'=>$t,
        ));

        }

        

        return $this->render('evaluacion/show.html.twig', array(
            //'pagination'=>$pagination,
            'evaluaciones'=>$evaluaciones1,
            'editForm'=>$editForm->createView(),
            'tamaño'=>$tamaño,
            'año'=>$año,
            'mes'=>$mes,
            't'=>$t,
        ));


    }

    /**
     * Displays a form to edit an existing evaluacion entity.
     *
     */
    public function editAction(Evaluacion $evaluacion)
    {
        $deleteForm = $this->createDeleteForm($evaluacion);
         $em = $this->getDoctrine()->getManager();
         $grupoDeEvaluacion = $em->getRepository('AcademiainscripcionBundle:Grupo')->findBy(array("id"=>$evaluacion->getIdGrupo()->getId() ));       

        return $this->render('evaluacion/edit.html.twig', array(
            'evaluacion' => $evaluacion,
            'delete_form' => $deleteForm->createView(),
            'grupoDeEvaluacion'=>$grupoDeEvaluacion[0],
        ));
    }

    /**
     * Deletes a evaluacion entity.
     *
     */
    public function deleteAction(Request $request, Evaluacion $evaluacion)
    {   
        //$id=$evaluacion->getId();
       //$grupo = $this->doctrine->em->find('AcademiainscripcionBundle:Grupo', $id);

        $form = $this->createDeleteForm($evaluacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($evaluacion);
            $em->flush();
        }

        return $this->redirectToRoute('evaluacion_show',array('id'=>$evaluacion->getIdGrupo()->getId()));
    }

    /**
     * Creates a form to delete a evaluacion entity.
     *
     * @param Evaluacion $evaluacion The evaluacion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Evaluacion $evaluacion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('evaluacion_delete', array('id' => $evaluacion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function obtenerAction(Request $request){
  
        $idDelGrupo=$request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $evaluacion = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => 1 ));  
        
        if($request->isXmlHttpRequest()){

        $jsonData= array();
        $idx=0;
          foreach ($evaluacion as $evaluacion) {
            $temp = array(
                'id' => $evaluacion->getId(),
                'nombre'=>$evaluacion->getNombre(),
                'ponderacion'=>$evaluacion->getPonderacion(),
            );  

                $jsonData[$idx++] = $temp;
        

            }
            return new JsonResponse($jsonData);
    }else { 
      return $this->render('student/ajax.html.twig'); 
   } 
    }


public function crearAction(Grupo $grupo,Request $request){
$em = $this->getDoctrine()->getManager();
$evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $grupo->getId() ));
$totalPonderaciones=0;
$mes=1;
$anio=1000;
$datosCorrectos=0;

        $eval=array();
        $i=0;
        


        if (isset($_POST["guardar"])) {
           
            $mes=$request->request->get("mes");
            $anio=$request->request->get("anio");
            $bandera=0;

            $mesC=$this->obtenerMes($mes);

            foreach ($evaluaciones as  $evaluacions) {
            
                $totalPonderaciones=$evaluacions->getPonderacion()+$totalPonderaciones;
                $evaluacionAño=$evaluacions->getPeriodoAño();
                $evaluacionMes=$evaluacions->getPeriodoMes();
                $i++;
                if ($mes== $evaluacionMes && $anio == $evaluacionAño) {
                  # code...
                  $bandera=1;
                }
        }
            if ($bandera==0) {
              # code...
            
                   for ($i=1; $i < 11; $i++) { 
                      $ponderaciones=$request->request->get($i+10);
                      $nombre=$request->request->get($i);
                    
                  
                      if ($nombre != "" && $ponderaciones != "") {
                        # code...
                      
                      $evaluaciones=new Evaluacion();
                      $evaluaciones->setNombre($nombre);
                      $evaluaciones->setPonderacion($ponderaciones);
                      $evaluaciones->setIdGrupo($grupo);
                      $evaluaciones->setPeriodoMes($mes);
                      $evaluaciones->setPeriodoAño($anio);
                      $em->persist($evaluaciones);
                      $em->flush();
                      }
                   }
                   $datosCorrectos=2;
                    return $this->render('evaluacion/new.html.twig', array(
                      'evaluaciones' => $evaluaciones,
                      'totalPonderaciones'=>$totalPonderaciones,
                      'grupo'=>$grupo,
                      'datosCorrectos'=>$datosCorrectos,
                      'mes'=>$mesC,
                      'anio'=>$anio,

        ));
              }else{
                $datosCorrectos=3;
                return $this->render('evaluacion/new.html.twig', array(
                      'evaluaciones' => $evaluaciones,
                      'totalPonderaciones'=>$totalPonderaciones,
                      'grupo'=>$grupo,
                      'datosCorrectos'=>$datosCorrectos,
                      'mes'=>$mesC,
                      'anio'=>$anio,
                       ));
            }

          }

      


        return $this->render('evaluacion/new.html.twig', array(
            'evaluaciones' => $evaluaciones,
            'totalPonderaciones'=>$totalPonderaciones,
            'grupo'=>$grupo,
            'datosCorrectos'=>$datosCorrectos,
            'mes'=>$mes,
            'anio'=>$anio,

        ));
    }

    public static function obtenerMes($mes){
      $mesConver="";
switch ($mes) {
  case 1:
    $mesConver="Enero";
    break;
  case 2:
    $mesConver="Febrero";
    break;
    case 3:
    $mesConver="Marzo";
    break;
    case 4:
    $mesConver="Abril";
    break;
    case 5:
    $mesConver="Mayo";
    break;
    case 6:
    $mesConver="Junio";
    break;
    case 9:
    $mesConver="Julio";
    break;
    case 8:
    $mesConver="Agosto";
    break;
    case 9:
    $mesConver="Septiembre";
    break;
    case 10:
    $mesConver="Octubre";
    break;
    case 11:
    $mesConver="Noviembre";
    break;
    case 12:
    $mesConver="Diciembre";
    break;
  default:
    # code...
    break;
}
return $mesConver;
    }


}