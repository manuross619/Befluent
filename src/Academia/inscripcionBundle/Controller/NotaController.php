<?php

namespace Academia\inscripcionBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

use Academia\inscripcionBundle\Entity\Nota;
use Academia\inscripcionBundle\Entity\Grupo;
use Academia\inscripcionBundle\Entity\Evaluacion;
use Academia\inscripcionBundle\Entity\Estudiante;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Docente controller.
 *
 */
class NotaController extends Controller
{
    /**
     * Lists all docente entities.
     *
     */
    public function nuevaAction(Request $request,$id,Estudiante $idEstudiante)
    {

        $em = $this->getDoctrine()->getManager();
        $now = new  \DateTime();
        $año=$now->format('Y');
        $mes=$now->format('m');
        $mesC=$this->obtenerMes($mes); 
        $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $id,'periodoAño'=>$año,'periodoMes'=>$mes));
        $grupoInscrito=$em->getRepository('AcademiainscripcionBundle:Grupo')->find($id);
        $notasAux=array();
        $evaluacionesConNotas=array();
        $evaluacionesSinNotas=array();
        $i=0;
        $u=0;
        $now = new  \DateTime();
        

        foreach ($evaluaciones as $evaluacion) { 
            # code...
            $notas = $em->getRepository('AcademiainscripcionBundle:Nota')->findBy(array('evaluacion' => $evaluacion->getId(), 'estudiante'=>$idEstudiante->getId()));
           
            foreach ($notas as $nota) {
                # 
                $notasAux[$i]=$nota;
                $evaluacionesConNotas[$i]=$evaluacion;
                $i++;

            }
            if (sizeof($notas)==0) {
                # code...
                $evaluacionesSinNotas[$u]=$evaluacion;
                $u++;
            }
            
        }
        $o=0;//No sirve
       
        $n=sizeof($evaluacionesSinNotas);
        $notasIngresadas=sizeof($notasAux);
        $totalEvaluaciones=sizeof($evaluaciones);
        $tamañoNotas=sizeof($notasAux);
        if ($tamañoNotas>0) {
                $tamañoNotas--;
            # code...
        }

        $tamañoEvaluaciones=sizeof($evaluaciones)-1;
        

        foreach ($evaluaciones as $evaluacion) {
            # code...
            foreach ($notasAux as $notaAux) {
                if ($evaluacion->getId()==$notaAux->getEvaluacion()->getId()) {
                    # code...
                    $evaluacionConNotas[$o]=$evaluacion;
                }
            }
        }   
//------------------------------------------------------------------------------------------------------>>>>>>>>
        if (isset($_POST["buscar"])) {
            $mes=$request->request->get("mes");
            $año=$request->request->get("anio");
            $mesC=$this->obtenerMes($mes);
            $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $id,'periodoAño'=>$año,'periodoMes'=>$mes));
            $grupoInscrito=$em->getRepository('AcademiainscripcionBundle:Grupo')->find($id);
            $notasAux=array();
            $i=0;
            $u=0;
            $now = new  \DateTime();
            

        foreach ($evaluaciones as $evaluacion) { 
            # code...
            $notas = $em->getRepository('AcademiainscripcionBundle:Nota')->findBy(array('evaluacion' => $evaluacion->getId(), 'estudiante'=>$idEstudiante->getId()));
            
            foreach ($notas as $nota) {
                # 
                $notasAux[$i]=$nota;
                $evaluacionesConNotas[$o]=$evaluacion;
                $i++;
            }
             if (sizeof($notas)==0) {
                $evaluacionesSinNotas[$u]=$evaluacion;
            }
            $u++;
            
        }
        $o=0;
       
        $notasIngresadas=sizeof($notasAux);
        $totalEvaluaciones=sizeof($evaluaciones);
        $tamañoNotas=sizeof($notasAux);
        if ($tamañoNotas>0) {
                $tamañoNotas--;
            # code...
        }

        $tamañoEvaluaciones=sizeof($evaluaciones)-1;

        return $this->render('nota/new.html.twig', array(
            'evaluaciones'=>$evaluaciones,
            'idEstudiante'=>$idEstudiante,
            'notasAux'=>$notasAux,
            'tamañoNotas'=>$tamañoNotas,
            'notasIngresadas'=>$notasIngresadas,
            'tamañoEvaluaciones'=>$tamañoEvaluaciones,
            'totalEvaluaciones'=>$totalEvaluaciones,
            'grupoInscrito'=>$grupoInscrito,
            'año'=>$año,
            'mes'=>$mes,
            'mesNombre'=>$mesC,
            'id'=>$id,
            'esn'=>$evaluacionesSinNotas,
            'ecn'=>$evaluacionesConNotas,
            'n'=>$n
            ));

        }

         if (isset($_POST["guardar"])) {
            $i=0;
            $mes=$request->request->get("mes");
            $año=$request->request->get("anio");
            $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $id,'periodoAño'=>$año,'periodoMes'=>$mes));
            foreach ($evaluaciones as $valor) {
                $nota = new Nota();
                $evaluacion=$evaluaciones;
                $valores=$request->request->get($valor->getId());
                if ($valores != null) {
                    # code...
                
                $nota->setValor($valores);
                $nota->setEvaluacion($valor);
                $nota->setEstudiante($idEstudiante);
                $em->persist($nota);
                $em->flush();
                $i++;
            }
            }

        $db = $em->getConnection();
        $query = "SELECT e.id, e.nombre, e.edad, e.dui, e.telefono, e.email from estudiante e INNER JOIN estudiante_grupo ge ON e.id = ge.estudiante_id INNER JOIN grupo g ON g.id = ge.grupo_id WHERE g.id = $id";
        
        $stmt = $db->prepare($query);
        $params = array();
        $stmt->execute($params);
        $po=$stmt->fetchAll();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($po, $request->query->getInt('page', 1), 5);

        return $this->render('estudiante/listadoPorGrupo.html.twig', array(
            'pagination' => $pagination,
            'id'=>$id
        ));

         }else{

        return $this->render('nota/new.html.twig', array(
            'evaluaciones'=>$evaluaciones,
            'idEstudiante'=>$idEstudiante,
            'notasAux'=>$notasAux,
            'tamañoNotas'=>$tamañoNotas,
            'notasIngresadas'=>$notasIngresadas,
            'tamañoEvaluaciones'=>$tamañoEvaluaciones,
            'totalEvaluaciones'=>$totalEvaluaciones,
            'grupoInscrito'=>$grupoInscrito,
            'año'=>$año,
            'mes'=>$mes,
            'mesNombre'=>$mesC,
            'id'=>$id,
            'esn'=>$evaluacionesSinNotas,
            'ecn'=>$evaluacionesConNotas,
            'n'=>$n
            ));
    }

    }






//------------------------------------  MODIFICAR ------------------------------------------------------------------------------------------------------
    public function modificarAction(Request $request,$id,Estudiante $idEstudiante){

        $em = $this->getDoctrine()->getManager();
        $now = new  \DateTime();
        $año=$now->format('Y');
        $mes=$now->format('Y');
        $mesC=$this->obtenerMes($mes);
        $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $id,'periodoAño'=>$año,'periodoMes'=>$mes));
        $grupoInscrito=$em->getRepository('AcademiainscripcionBundle:Grupo')->find($id);
        $notasAux=array();
        $evaluacionesSinNotas=array();
        $i=0;
        if (isset($_POST["guardar"])) {
            $mes=$request->request->get("mes");
            $año=$request->request->get("anio");
             $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $id,'periodoAño'=>$año,'periodoMes'=>$mes));
            foreach ($evaluaciones as $evaluacion) { 
            # code...
                $notas = $em->getRepository('AcademiainscripcionBundle:Nota')->findBy(array('evaluacion' => $evaluacion->getId(), 'estudiante'=>$idEstudiante->getId()));

                foreach ($notas as $nota) {
                # 
                    $notasAux[$i]=$nota;
                    $i++;
                    $valores=$request->request->get($evaluacion->getId());
                    $nota->setValor($valores);
                    $em->flush();
                    if (sizeof($notas)==0) {
                        $evaluacionesSinNotas[$u]=$evaluacion;
                        $u++;
            }
                }

       
            }            
        
        }

            if (isset($_POST["buscar"])){

                $mes=$request->request->get("mes");
                $año=$request->request->get("anio");
                $mesC=$this->obtenerMes($mes);
                $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $id,'periodoAño'=>$año,'periodoMes'=>$mes));
                $grupoInscrito=$em->getRepository('AcademiainscripcionBundle:Grupo')->find($id);
                $notasAux=array();
                $i=0;
            

                foreach ($evaluaciones as $evaluacion) { 
                    $notas = $em->getRepository('AcademiainscripcionBundle:Nota')->findBy(array('evaluacion' => $evaluacion->getId(), 'estudiante'=>$idEstudiante->getId()));
            
                    foreach ($notas as $nota) {
                # 
                        $notasAux[$i]=$nota;
                        $i++;
                        if (sizeof($notas)==0) {
                            $evaluacionesSinNotas[$u]=$evaluacion;
                            $u++;
                        }
                    }
            
                }
                $o=0;
       
                $notasIngresadas=sizeof($notasAux);
                $totalEvaluaciones=sizeof($evaluaciones);
                $tamañoNotas=sizeof($notasAux);
                if ($tamañoNotas>0) {
                    $tamañoNotas--;
            # code...
                }

                $tamañoEvaluaciones=sizeof($evaluaciones)-1;

                return $this->render('nota/edit.html.twig', array(
                    'evaluaciones'=>$evaluaciones,
                    'idEstudiante'=>$idEstudiante,
                    'notasAux'=>$notasAux,
                    'tamañoNotas'=>$tamañoNotas,
                    'notasIngresadas'=>$notasIngresadas,
                    'tamañoEvaluaciones'=>$tamañoEvaluaciones,
                    'totalEvaluaciones'=>$totalEvaluaciones,
                    'grupoInscrito'=>$grupoInscrito,
                    'año'=>$año,
                    'mes'=>$mes,
                    'mesNombre'=>$mesC,
                    'esn'=>$evaluacionesSinNotas,
                ));

            }
        $o=0;
       
        $notasIngresadas=sizeof($notasAux);
        $tamañoNotas=sizeof($notasAux);
        $totalEvaluaciones=sizeof($evaluaciones);
        if ($tamañoNotas>0) {
                $tamañoNotas--;
            # code...
        }

        $tamañoEvaluaciones=sizeof($evaluaciones)-1;


        return $this->render('nota/edit.html.twig', array(
            'evaluaciones'=>$evaluaciones,
            'idEstudiante'=>$idEstudiante,
            'notasAux'=>$notasAux,
            'tamañoNotas'=>$tamañoNotas,
            'notasIngresadas'=>$notasIngresadas,
            'tamañoEvaluaciones'=>$tamañoEvaluaciones,
            'totalEvaluaciones'=>$totalEvaluaciones,
            'grupoInscrito'=>$grupoInscrito,
            'año'=>$año,
            'mes'=>$mes,
            'mesNombre'=>$mesC,
            'esn'=>$evaluacionesSinNotas,
        ));
    

    }

    public function mostrarAction(Request $request,$id,Estudiante $idEstudiante){

        $em = $this->getDoctrine()->getManager();
        $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $id));
        $grupoInscrito=$em->getRepository('AcademiainscripcionBundle:Grupo')->find($id);
        $notasAux=array();
        $i=0;
        foreach ($evaluaciones as $evaluacion) { 
            # code...
            $notas = $em->getRepository('AcademiainscripcionBundle:Nota')->findBy(array('evaluacion' => $evaluacion->getId(), 'estudiante'=>$idEstudiante->getId()));

            foreach ($notas as $nota) {
                # 
                $notasAux[$i]=$nota;
                $i++;
            }
        
        }

        $notasIngresadas=sizeof($notasAux);
        $tamañoNotas=sizeof($notasAux);
        $totalEvaluaciones=sizeof($evaluaciones);
        $tamañoEvaluaciones=sizeof($evaluaciones)-1;
        if ($tamañoNotas>0) {
                $tamañoNotas--;
            # code...
        }

        if (isset($_POST["regresar"])) {
            $db = $em->getConnection();
            $query = "SELECT e.id, e.nombre, e.edad, e.dui, e.telefono, e.email from estudiante e INNER JOIN estudiante_grupo ge ON e.id = ge.estudiante_id INNER JOIN grupo g ON g.id = ge.grupo_id WHERE g.id = $id";
        
            $stmt = $db->prepare($query);
            $params = array();
            $stmt->execute($params);
            $po=$stmt->fetchAll();
        
            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate($po, $request->query->getInt('page', 1), 5);

        return $this->render('estudiante/listadoPorGrupo.html.twig', array(
            'pagination' => $pagination,
            'id'=>$id
        ));
        }

        return $this->render('nota/show.html.twig', array(
            'evaluaciones'=>$evaluaciones,
            'idEstudiante'=>$idEstudiante,
            'notasAux'=>$notasAux,
            'tamañoNotas'=>$tamañoNotas,
            'notasIngresadas'=>$notasIngresadas,
            'tamañoEvaluaciones'=>$tamañoEvaluaciones,
            'totalEvaluaciones'=>$totalEvaluaciones,
            'grupoInscrito'=>$grupoInscrito,
        ));
    }

    public function verNotasAction(Request $request,$id){
        $em = $this->getDoctrine()->getManager();
        $now = new  \DateTime();
        $i=0;
        $u=0;
        $l=0;
        $notasAux=array();
        $evaluacionesSinNotas=array();
        $evaluacionesConNotas=array();

        if (isset($_POST["buscar"])) {
            $mes=$request->request->get("mes");
            $año=$request->request->get("anio");
            $mesC=$this->obtenerMes($mes);
            $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $id,'periodoAño'=>$año,'periodoMes'=>$mes));
            $grupoInscrito=$em->getRepository('AcademiainscripcionBundle:Grupo')->find($id);
            $notasAux=array();
            $i=0;
            $u=0;
            $o=0;
            $l=0;
           
            $evaluacionesSinNotas=array();
            $evaluacionesConNotas=array();
            $db = $em->getConnection();
            $query = "SELECT e.id, e.nombre, e.edad, e.dui, e.telefono, e.email from estudiante e INNER JOIN estudiante_grupo ge ON e.id = ge.estudiante_id INNER JOIN grupo g ON g.id = ge.grupo_id WHERE g.id = $id";
            $stmt = $db->prepare($query);
            $params = array();
            $stmt->execute($params);
            $estudiantesDelGrupo=$stmt->fetchAll();

        
            foreach ($evaluaciones as $evaluacion) { 
            # code...
            $notas = $em->getRepository('AcademiainscripcionBundle:Nota')->findBy(array('evaluacion' => $evaluacion->getId()));
            foreach ($notas as $nota) {
                # 
                $notasAux[$i]=$nota;
                $i++;

                 if (sizeof($notas)==0) {
                        $evaluacionesSinNotas[$u]=$evaluacion;
                        $u++;
            }else{
                $evaluacionesConNotas[$l]=$evaluacion;
                $l++;
            }
            }
        }

        $o=0;
       
            $notasIngresadas=sizeof($notasAux);
            $totalAlumnos=sizeof($estudiantesDelGrupo);
            $arregloAlumnos=sizeof($estudiantesDelGrupo);
            if ($arregloAlumnos>0) {
                $arregloAlumnos--;
            }
            $totalEvaluaciones=sizeof($evaluaciones);
            $tamañoNotas=sizeof($notasAux);
            if ($tamañoNotas>0) {
                    $tamañoNotas--;
            # code...
                }
            
            $tamañoNotas=sizeof($notasAux);
            $totalEvaluaciones=sizeof($evaluaciones);
            if ($tamañoNotas>0) {
                $tamañoNotas--;
            # code...
        }

            $tamañoEvaluaciones=sizeof($evaluaciones)-1;

            $totalECN=sizeof($evaluacionesConNotas);
            $arregloECN=sizeof($evaluacionesConNotas);
            if ($arregloECN>0) {
                $arregloECN--;
            }


            return $this->render('nota/notasEstudiantes.html.twig', array(
                'evaluaciones'=>$evaluaciones,
                'estudiantes'=>$estudiantesDelGrupo,
                'notasAux'=>$notasAux,
                'tamañoNotas'=>$tamañoNotas,
                'notasIngresadas'=>$notasIngresadas,
                'tamañoEvaluaciones'=>$tamañoEvaluaciones,
                'totalEvaluaciones'=>$totalEvaluaciones,
                'arregloAlumnos'=>$arregloAlumnos,
                'año'=>$año,
                'mes'=>$mes,
                'mesNombre'=>$mesC,
                'esn'=>$evaluacionesSinNotas,
                'ecn'=>$evaluacionesConNotas,
                'totalAlumnos'=>$totalAlumnos,
                'arregloECN'=>$arregloECN,
                'totalECN'=>$totalECN,
                'id'=>$id,
            ));
    

        }






        $db = $em->getConnection();
        $query = "SELECT e.id, e.nombre, e.edad, e.dui, e.telefono, e.email from estudiante e INNER JOIN estudiante_grupo ge ON e.id = ge.estudiante_id INNER JOIN grupo g ON g.id = ge.grupo_id WHERE g.id = $id";
        $stmt = $db->prepare($query);
        $params = array();
        $stmt->execute($params);
        $estudiantesDelGrupo=$stmt->fetchAll();

        $año=$now->format('Y');
        $mes=$now->format('m');
        $mesC=$this->obtenerMes($mes);
        $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $id,'periodoAño'=>$año,'periodoMes'=>$mes));

        foreach ($evaluaciones as $evaluacion) { 
            # code...
            $notas = $em->getRepository('AcademiainscripcionBundle:Nota')->findBy(array('evaluacion' => $evaluacion->getId()));
            foreach ($notas as $nota) {
                # 
                $notasAux[$i]=$nota;
                $i++;

                 if (sizeof($notas)==0) {
                        $evaluacionesSinNotas[$u]=$evaluacion;
                        $u++;
            }else{
                $evaluacionesConNotas[$l]=$evaluacion;
                $l++;
            }
            }
        }




        $notasIngresadas=sizeof($notasAux);
        $totalAlumnos=sizeof($estudiantesDelGrupo);
        $arregloAlumnos=sizeof($estudiantesDelGrupo);
        if ($arregloAlumnos>0) {
            $arregloAlumnos--;
        }
        $totalEvaluaciones=sizeof($evaluaciones);
        $tamañoNotas=sizeof($notasAux);
        if ($tamañoNotas>0) {
                    $tamañoNotas--;
            # code...
                }
        $notasIngresadas=sizeof($notasAux);
        $tamañoNotas=sizeof($notasAux);
        $totalEvaluaciones=sizeof($evaluaciones);
        if ($tamañoNotas>0) {
                $tamañoNotas--;
            # code...
        }

        $tamañoEvaluaciones=sizeof($evaluaciones)-1;

        $totalECN=sizeof($evaluacionesConNotas);
        $arregloECN=sizeof($evaluacionesConNotas);
        if ($arregloECN>0) {
            $arregloECN--;
        }
        


    
    return $this->render('nota/notasEstudiantes.html.twig', array(
            'evaluaciones'=>$evaluaciones,
            'estudiantes'=>$estudiantesDelGrupo,
            'notasAux'=>$notasAux,
            'tamañoNotas'=>$tamañoNotas,
            'notasIngresadas'=>$notasIngresadas,
            'tamañoEvaluaciones'=>$tamañoEvaluaciones,
            'totalEvaluaciones'=>$totalEvaluaciones,
            'arregloAlumnos'=>$arregloAlumnos,
            'año'=>$año,
            'mes'=>$mes,
            'mesNombre'=>$mesC,
            'esn'=>$evaluacionesSinNotas,
            'ecn'=>$evaluacionesConNotas,
            'totalAlumnos'=>$totalAlumnos,
            'arregloECN'=>$arregloECN,
            'totalECN'=>$totalECN,
            'id'=>$id,
        ));
    

    }

       public static function obtenerMes($mes){
      $mesConver="";
switch ($mes) {
  case 1:
    $mesConver="Enero";
    break;
  case 2:
    $mesConver="Febrero";
    break;
    case 3:
    $mesConver="Marzo";
    break;
    case 4:
    $mesConver="Abril";
    break;
    case 5:
    $mesConver="Mayo";
    break;
    case 6:
    $mesConver="Junio";
    break;
    case 9:
    $mesConver="Julio";
    break;
    case 8:
    $mesConver="Agosto";
    break;
    case 9:
    $mesConver="Septiembre";
    break;
    case 10:
    $mesConver="Octubre";
    break;
    case 11:
    $mesConver="Noviembre";
    break;
    case 12:
    $mesConver="Diciembre";
    break;
  default:
    # code...
    break;
}
return $mesConver;
    }


// metodo para generar pdf
    public function pdfNotaAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $now = new  \DateTime();

        $i=0;
        $u=0;
        $l=0;
        $notasAux=array();
        $evaluacionesSinNotas=array();
        $evaluacionesConNotas=array();
        //$id=$request->request->get("idgruoo");
        $mes=$request->request->get("mes");
        $año=$request->request->get("anio");
        $mesC=$this->obtenerMes($mes);
        $evaluaciones = $em->getRepository('AcademiainscripcionBundle:Evaluacion')->findBy(array('idGrupo' => $id,'periodoAño'=>$año,'periodoMes'=>$mes));
        $grupoInscrito=$em->getRepository('AcademiainscripcionBundle:Grupo')->find($id);
        $notasAux=array();
            $i=0;
            $u=0;
            $o=0;
            $l=0;
           
            $evaluacionesSinNotas=array();
            $evaluacionesConNotas=array();
            $db = $em->getConnection();
            $query = "SELECT e.id, e.nombre, e.edad, e.dui, e.telefono, e.email from estudiante e INNER JOIN estudiante_grupo ge ON e.id = ge.estudiante_id INNER JOIN grupo g ON g.id = ge.grupo_id WHERE g.id = $id";
            $stmt = $db->prepare($query);
            $params = array();
            $stmt->execute($params);
            $estudiantesDelGrupo=$stmt->fetchAll();

        
            foreach ($evaluaciones as $evaluacion) { 
            # code...
            $notas = $em->getRepository('AcademiainscripcionBundle:Nota')->findBy(array('evaluacion' => $evaluacion->getId()));
            foreach ($notas as $nota) {
                # 
                $notasAux[$i]=$nota;
                $i++;

                 if (sizeof($notas)==0) {
                        $evaluacionesSinNotas[$u]=$evaluacion;
                        $u++;
            }else{
                $evaluacionesConNotas[$l]=$evaluacion;
                $l++;
            }
            }
        }

        $o=0;
       
            $notasIngresadas=sizeof($notasAux);
            $totalAlumnos=sizeof($estudiantesDelGrupo);
            $arregloAlumnos=sizeof($estudiantesDelGrupo);
            if ($arregloAlumnos>0) {
                $arregloAlumnos--;
            }
            $totalEvaluaciones=sizeof($evaluaciones);
            $tamañoNotas=sizeof($notasAux);
            if ($tamañoNotas>0) {
                    $tamañoNotas--;
            # code...
                }
            
            $tamañoNotas=sizeof($notasAux);
            $totalEvaluaciones=sizeof($evaluaciones);
            if ($tamañoNotas>0) {
                $tamañoNotas--;
            # code...
        }

            $tamañoEvaluaciones=sizeof($evaluaciones)-1;

            $totalECN=sizeof($evaluacionesConNotas);
            $arregloECN=sizeof($evaluacionesConNotas);
            if ($arregloECN>0) {
                $arregloECN--;
            }


            $path = $request->server->get('DOCUMENT_ROOT');
            $html = $this->render('nota/pdfnotasEstudiantes.html.twig', array(
                'estudiantes'=>$estudiantesDelGrupo,
                'notasAux'=>$notasAux,
                'notasIngresadas'=>$notasIngresadas,
                'arregloAlumnos'=>$arregloAlumnos,
                'ecn'=>$evaluacionesConNotas,
                'totalAlumnos'=>$totalAlumnos,
                'arregloECN'=>$arregloECN,
                'totalECN'=>$totalECN,
                'path' => $path,
            ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'enable-javascript' => true,
                'javascript-delay' => 1000,
                'no-stop-slow-scripts' => true,
                'no-background' => false,
                'lowquality' => false,
                'encoding' => 'utf-8',
                'images' => true,
                'header-right'=>'Pag. [page] de [toPage]',
                'header-font-size'=>7,
                'footer-center'=>'Be Fluent language center'       
            )),200,array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="notas.pdf"'
            )

        );

    }







}