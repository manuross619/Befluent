<?php

namespace Academia\inscripcionBundle\Controller;

use Academia\inscripcionBundle\Entity\Docente;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Docente controller.
 *
 */
class DocenteController extends Controller
{
    /**
     * Lists all docente entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        //$docentes = $em->getRepository('AcademiainscripcionBundle:Docente')->findAll();

        $dql="SELECT d FROM AcademiainscripcionBundle:Docente d";
        $docentes= $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($docentes, $request->query->getInt('page', 1), 5);

        return $this->render('docente/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new docente entity.
     *
     */
    public function newAction(Request $request)
    {
        $docente = new Docente();
        $form = $this->createForm('Academia\inscripcionBundle\Form\DocenteType', $docente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($docente);
            $em->flush();

            return $this->redirectToRoute('docente_show', array('iddocente' => $docente->getIddocente()));
        }

        return $this->render('docente/new.html.twig', array(
            'docente' => $docente,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a docente entity.
     *
     */
    public function showAction(Docente $docente)
    {
        $deleteForm = $this->createDeleteForm($docente);

        return $this->render('docente/show.html.twig', array(
            'docente' => $docente,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing docente entity.
     *
     */
    public function editAction(Request $request, Docente $docente)
    {
        $deleteForm = $this->createDeleteForm($docente);
        $editForm = $this->createForm('Academia\inscripcionBundle\Form\DocenteType', $docente);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('docente_edit', array('iddocente' => $docente->getIddocente()));
        }

        return $this->render('docente/edit.html.twig', array(
            'docente' => $docente,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a docente entity.
     *
     */
    public function deleteAction(Request $request, Docente $docente)
    {
        $form = $this->createDeleteForm($docente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($docente);
            $em->flush();
        }

        return $this->redirectToRoute('docente_index');
    }

    /**
     * Creates a form to delete a docente entity.
     *
     * @param Docente $docente The docente entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Docente $docente)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('docente_delete', array('iddocente' => $docente->getIddocente())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

       public function countAction(Request $request, Docente $docente)
    {
        
    }

}
