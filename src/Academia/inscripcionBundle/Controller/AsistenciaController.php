<?php

namespace Academia\inscripcionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AsistenciaController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction()
    {

        return $this->render('asistencia/index.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/edit")
     */
    public function editAction()
    {
        return $this->render('asistencia/edit.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/show")
     */
    public function showAction()
    {
        return $this->render('asistencia/show.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/delete")
     */
    public function deleteAction()
    {
        return $this->render('asistencia/delete.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("new")
     */
    public function newAction()
    {
        return $this->render('asistencia/new.html.twig', array(
            // ...
        ));
    }

    public function buscarPorFechaAsistenciaAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $fechainicio=$request->request->get('fechainicio');
        $fechafin=$request->request->get('fechafin');
        $curso=$request->request->get('curso');

        $dql=" SELECT e.nombre AS nombre,
        asis.tipe_id AS tipo
        from AcademiaInscripcionBundle: Asistencia asis
        INNER JOIN AcademiaInscripcionBundle: Grupo gr
        with asis.grupo_id=gr.id
        INNER JOIN AcademiaInscripcionBundle: Estudiante e
        with asis.estud_id=e.id
        where asis.fecha BETWEEN '$fechainicio' and '$fechafin' and gr.tipo='$curso'";

        $asistencia = $em->createQuery($dql)->getResult();


        $response = array( 
			"code" => 200,
			"boton" => $this->render('AcademiainscripcionBundle:Asistencia:botonAsistencia.html.twig', array('fechainicio' => $request->get('fechainicio'),
            "fechafin" => $request->get('fechafin'),
            "curso" => $request->get('curso')))->getContent(),
			"response" => $this->render('AcademiainscripcionBundle:Asistencia:documentoAsistencia.html.twig',array('asistencia'=>$asistencia))->getContent());
			return new JsonResponse($response);

    }

    public function generarDocumentoAsistenciaAction($fechainicio,$fechafin,$curso, Request $request){

        $em = $this->getDoctrine()->getManager();
        $path = $request->server->get('DOCUMENT_ROOT');

        $dql=" SELECT e.nombre AS nombre,
        asis.tipe_id AS tipo
        from AcademiaInscripcionBundle: Asistencia asis
        INNER JOIN AcademiaInscripcionBundle: Grupo gr
        with asis.grupo_id=gr.id
        INNER JOIN AcademiaInscripcionBundle: Estudiante e
        with asis.estud_id=e.id
        where asis.fecha BETWEEN '$fechainicio' and '$fechafin' and gr.tipo='$curso'";

        $asistencia = $em->createQuery($dql)->getResult();
        





        $html = $this->renderView('AcademiainscripcionBundle:Asistencia:pdfasistencia.html.twig', array('asistencia'=>$asistencia, 'path' => $path));

		return new Response(
				$this->get('knp_snappy.pdf')->getOutputFromHtml($html, array( 
					'enable-javascript' => true, 
					'javascript-delay' => 1000, 
					'no-stop-slow-scripts' => true, 
					'no-background' => false, 
					'lowquality' => false,
					'encoding' => 'utf-8',
					'images' => true,
					'header-right'=>'Pag. [page] de [toPage]',
					'header-font-size'=>7,
				)),200,array(
				'Content-Type' => 'application/pdf',
				'Content-Disposition' => 'attachment; filename="asistencia.pdf"'
			)
		);

    }



}
